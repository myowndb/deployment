#!/bin/bash

if [[ $# -eq 0 ]]; then 
	cat <<-END
 Script to ease the definitions of record types from postgresql tables for use with Dapper
 It works on the output from the \`\d\` command
 First argument is the module name, second is the table name.
 It will output its result on stdout. Here's an example:

 # pg2fs.sh MyModule users
 ----- Start output -----
  module MyModule
 
  type MyModule = {
  id: 	int
  name: text
  }
 ----- End output -----
END
exit 1
fi

module=$1
table=$2
# The database to query
database="myowndb"
# The psql command to use. Change it to use the options and flags you want.
psql="p"

echo "module $module"
# open System for DateTime
echo "open System"
echo 
echo "type $module = {"

$psql $database -c "\d $table" \
	| tail -n +4  \
	| sed -n '/^Indexes:/q;p' \
	| awk -F '\\|' '{print $1 ":" $2}' \
	| sed -e 's/integer/int/g;
	          s/character.*/text/g;
		  s/text/string/g;
		  s/timestamp.*/DateTime/g;
		  s/: date/: DateTime/g;
		  s/double precision/float/g;
		  s/bigint/int/g;
		  s/boolean/Boolean/g;
		  s/\s*$//g'


echo "}"
