hosts=(myowndb)
ips_file="vars/ips"

# collect ips of $hosts in vars/ips.{sh,yml}
init_ips() {
	local ip
	cat >$ips_file.sh <<-END
	# declare associative array
	declare -A ips
	END
	echo "" >$ips_file.yml
	for host in $hosts; do
		ip=$(lxc info $host |  grep eth0 | head -n 1 | awk '{print $3}')
		cat >>$ips_file.sh <<-END
		ips[$host]=$ip
		END
		cat >>$ips_file.yml <<-END
		$host:$ip
		END
	done
}

# generate ansible_hosts
init_hosts() {
	. vars/ips.sh
	for host in $hosts; do
		cat >ansible_hosts <<-END
		$host ansible_host=${ips[$host]} ansible_user=ansible
		END
	done
	cat >>ansible_hosts <<-END
		[database]
		myowndb
		[web]
		myowndb
		[app]
		myowndb
	END

}

[[ -v BASE_DIR ]] && echo "ERROR : BASE_DIR variable is defined with value $BASE_DIR, not everriding it!!" && exit 1
export BASE_DIR=$PWD

init_ips
init_hosts
export ANSIBLE_INVENTORY=$PWD/ansible_hosts
export PATH=$PATH:$BASE_DIR/bin
